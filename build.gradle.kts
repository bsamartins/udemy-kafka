buildscript {
    repositories {
        gradlePluginPortal()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.0")
    }
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")

    repositories {
        mavenCentral()
    }

    dependencies {
        val implementation by configurations
        val runtimeOnly by configurations

        implementation(platform("com.fasterxml.jackson:jackson-bom:2.11.1"))

        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("reflect"))

        implementation("org.apache.kafka:kafka-clients:2.5.0")

        implementation("org.slf4j:slf4j-api:1.7.30")
        runtimeOnly("org.slf4j:log4j-over-slf4j:1.7.30")
        runtimeOnly("ch.qos.logback:logback-classic:1.0.0")

        constraints {
            add("implementation", "org.apache.kafka:kafka-streams:2.5.0")
        }
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}