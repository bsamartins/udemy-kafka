#!/bin/bash

kafka-topics --bootstrap-server localhost:9092 --create --topic color-preferences-raw \
  --partitions 1 --replication-factor 1

kafka-topics --bootstrap-server localhost:9092 --create --topic color-preferences-users \
  --partitions 1 --replication-factor 1 \
  --config cleanup.policy=compact

kafka-topics --bootstrap-server localhost:9092 --create --topic color-preferences-count \
  --partitions 1 --replication-factor 1  \
  --config cleanup.policy=compact

kafka-console-producer --broker-list localhost:9092 --topic color-preferences-raw

kafka-consumer-groups --bootstrap-server localhost:9092 --group streams-color-preferences --describe

kafka-topics --bootstrap-server localhost:9092 --delete --topic color-preferences-raw
kafka-topics --bootstrap-server localhost:9092 --delete --topic color-preferences-users
kafka-topics --bootstrap-server localhost:9092 --delete --topic color-preferences-count

# user1,red
# user2,blue
# user3,green
# user1,blue

# in the end table should contain values
# red -> 0
# green -> 1
# red -> 2