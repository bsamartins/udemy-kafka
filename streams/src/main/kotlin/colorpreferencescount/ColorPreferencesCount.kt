package colorpreferencescount

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.*
import org.apache.kafka.streams.kstream.Materialized
import org.apache.kafka.streams.kstream.Named
import org.apache.kafka.streams.kstream.Printed
import org.apache.kafka.streams.kstream.Produced
import java.util.*

const val GROUP_ID = "streams-color-preferences"
const val INPUT_TOPIC_NAME = "color-preferences-raw"
const val USERS_TOPIC_NAME = "color-preferences-users"
const val OUTPUT_TOPIC_NAME = "color-preferences-count"

val COLORS = setOf("green", "red", "blue")

fun main() {
    // properties
    val properties = Properties()
    properties[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:9092"
    properties[StreamsConfig.APPLICATION_ID_CONFIG] = GROUP_ID
    properties[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "earliest"

    val builder = StreamsBuilder()
    val inputTopic = builder.stream<String, String>(INPUT_TOPIC_NAME)

    val colorsTopic = inputTopic.map { _, v ->
        val s = v.split(",")
        KeyValue.pair(s[0], s[1])
    }.filter { _, value -> COLORS.contains(value) }

    colorsTopic.to(USERS_TOPIC_NAME)

    // Read from topic as table so updates are read correctly
    val table = builder.table<String,String>(USERS_TOPIC_NAME)
            .groupBy { _, value -> KeyValue.pair(value, value) }
            .count(Named.`as`("counts"), Materialized.with(Serdes.String(), Serdes.Long()))

    val stream = table.toStream()
    stream.print(Printed.toSysOut())
    stream.to(OUTPUT_TOPIC_NAME, Produced.with(Serdes.StringSerde(), Serdes.LongSerde()))

    val topology: Topology = builder.build()
    println(topology.describe())

    // build topology
    val kafkaStreams = KafkaStreams(topology, properties)

    // start application
    kafkaStreams.start()
    Runtime.getRuntime().addShutdownHook(Thread(kafkaStreams::close))
}