kafka-topics --bootstrap-server localhost:9092 --create --topic user-purchases \
  --partitions 3 --replication-factor 1

kafka-topics --bootstrap-server localhost:9092 --create --topic user-table \
  --partitions 2 --replication-factor 1 --config cleanup.policy=compact

kafka-topics --bootstrap-server localhost:9092 --create --topic user-purchases-enriched-left-join \
  --partitions 3 --replication-factor 1

kafka-topics --bootstrap-server localhost:9092 --create --topic user-purchases-enriched-inner-join \
  --partitions 3 --replication-factor 1