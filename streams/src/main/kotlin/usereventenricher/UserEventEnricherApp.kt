package usereventenricher

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.kstream.KeyValueMapper
import org.apache.kafka.streams.kstream.ValueJoiner
import java.util.*


fun main() {
    val properties = Properties()
    properties[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:9092"
    properties[StreamsConfig.APPLICATION_ID_CONFIG] = "user-event-enricher-app"
    properties[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
//    properties[StreamsConfig.PROCESSING_GUARANTEE_CONFIG] = StreamsConfig.EXACTLY_ONCE
    properties[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "earliest"

    val builder = StreamsBuilder()

    val userGlobalTable = builder.globalTable<String, String>("user-table")
    val userPurchases = builder.stream<String, String>("user-purchases")

    userPurchases.join(userGlobalTable,
            KeyValueMapper { key, _ -> key },
            ValueJoiner{ purchase: String, user: String -> "Purchase=$purchase,UserInfo=[$user]" })
            .to("user-purchases-enriched-inner-join")

    userPurchases.leftJoin(userGlobalTable,
            KeyValueMapper { key, _ -> key },
            ValueJoiner{ purchase: String, user: String? -> "Purchase=$purchase,UserInfo=[$user}]" })
            .to("user-purchases-enriched-left-join")

    val topology = builder.build()
    println(topology.describe())

    val kafkaStreams = KafkaStreams(topology, properties)
    kafkaStreams.start()
    Runtime.getRuntime().addShutdownHook(Thread(kafkaStreams::close))
}