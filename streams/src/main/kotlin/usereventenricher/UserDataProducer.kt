package usereventenricher

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.lang.Thread.sleep
import java.util.*

fun main() {
    val kafkaProducer = createKafkaProducer()

    println("Example 1 - new user")
    kafkaProducer.send(userRecord("john", User("John", "Doe", "john.doe@gmail.com"))).get()
    kafkaProducer.send(purchaseRecord("john", "Apples and Bananas (1)")).get()
    sleep(10000)

    println("Example 2 - non existing user")
    kafkaProducer.send(purchaseRecord("bob", "Kafka Udemy Course (2)")).get()
    sleep(10000)

    println("Example 3 - update user")
    kafkaProducer.send(userRecord("john", User("Johnny", "Doe", "johnny.doe@gmail.com"))).get()
    kafkaProducer.send(purchaseRecord("john", "Oranges (3)")).get()
    sleep(10000)

    println("Example 4 - non existing user then user")
    kafkaProducer.send(purchaseRecord("jane", "Computer (4)")).get()
    kafkaProducer.send(userRecord("jane", User("Jane", "Doe", "jane.doe@gmail.com"))).get()
    kafkaProducer.send(purchaseRecord("jane", "Books (4)")).get()
    kafkaProducer.send(userRecord("jane", null)).get()
    sleep(10000)

    println("Example 5 - user then delete data")
    kafkaProducer.send(userRecord("alice", User("Alice", "Doe", "alice.doe@gmail.com"))).get()
    kafkaProducer.send(userRecord("alice", null)).get()
    kafkaProducer.send(purchaseRecord("alice", "Apache Kafka Series (5)")).get()
    sleep(10000)
}

fun purchaseRecord(key: String, product: String) =
        ProducerRecord<String, String>("user-purchases", key, product)

fun userRecord(key: String, user: User?): ProducerRecord<String, String?> {
    val value: String? = if (user != null) {
        "FirstName=${user.firstName},LastName=${user.lastName},Email=${user.email}"
    } else {
        null
    }
    return ProducerRecord("user-table", key, value)
}

data class User(val firstName: String, val lastName: String, val email: String)

private fun createKafkaProducer(): KafkaProducer<String, String> {
    val properties = Properties()
    properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "127.0.0.1:9092"
    properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.qualifiedName
    properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.qualifiedName

    properties[ProducerConfig.ACKS_CONFIG] = "all"
    properties[ProducerConfig.RETRIES_CONFIG] = 3.toString()
    properties[ProducerConfig.LINGER_MS_CONFIG] = 1.toString()
    properties[ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG] = true.toString()

    return KafkaProducer(properties)
}
