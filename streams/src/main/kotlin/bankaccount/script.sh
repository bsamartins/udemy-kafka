#!/bin/bash

kafka-topics --bootstrap-server localhost:9092 --create --topic bank-transactions \
  --partitions 1 --replication-factor 1

kafka-consumer-groups --bootstrap-server localhost:9092 \
  --topic bank-transactions --group bank-account-balance --execute --reset-offsets --to-earliest