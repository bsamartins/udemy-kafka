package bankaccount

import java.time.LocalDateTime

data class BankAccountTransaction(val name: String,
                                  val amount: Int,
                                  val date: LocalDateTime)

class BankAccountBalance {

    var amount: Int = 0
        private set

    var date: LocalDateTime? = null
        private set

    fun add(amount: Int, date: LocalDateTime) {
        this.amount += amount
        this.date = date
    }

}