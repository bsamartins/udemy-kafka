package bankaccount

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.time.Duration
import java.time.LocalDateTime
import java.util.*

internal const val BANK_TOPIC = "bank-transactions"
val OBJECT_MAPPER: ObjectMapper = ObjectMapper()
        .registerModule(JavaTimeModule())
        .registerModule(KotlinModule())
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

private val users = listOf("Alice", "Bob", "Clare")

fun main() {
    val kafkaProducer = createKafkaProducer()
    val startTime = System.currentTimeMillis()
    var shouldRun = true
    val transactions: MutableMap<String, Int> = mutableMapOf()
    while(shouldRun && !Thread.interrupted()) {
        produceRandomTransaction(users.random(), kafkaProducer, transactions)
        Thread.sleep(100)
        shouldRun = (System.currentTimeMillis() - startTime) < Duration.ofSeconds(2).toMillis()
    }
    println("Done")
    transactions.forEach { (k, v) -> println("$k -> $v") }
}

private fun produceRandomTransaction(name: String, kafkaProducer: KafkaProducer<String, String>, transactions: MutableMap<String, Int>) {
    val transaction = randomTransaction(name)
    val json = OBJECT_MAPPER.writeValueAsString(transaction)
    val record = ProducerRecord(BANK_TOPIC, transaction.name, json)
    kafkaProducer.send(record)
    transactions.computeIfAbsent(name) { 0 }
    transactions.computeIfPresent(name) { _, v -> v + transaction.amount }
}

fun randomTransaction(name: String): BankAccountTransaction {
    val randomAmount = (0..100).random()
    return BankAccountTransaction(name, randomAmount, LocalDateTime.now())
}

private fun createKafkaProducer(): KafkaProducer<String, String> {
    val properties = Properties()
    properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "127.0.0.1:9092"
    properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.qualifiedName
    properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.qualifiedName

    // Safe producer
    properties[ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG] = true.toString()
    properties[ProducerConfig.ACKS_CONFIG] = "all"
    properties[ProducerConfig.RETRIES_CONFIG] = Integer.MAX_VALUE.toString()
    properties[ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION] = 5.toString()

    // compression
    properties[ProducerConfig.COMPRESSION_TYPE_CONFIG] = "snappy"
    properties[ProducerConfig.LINGER_MS_CONFIG] = 20.toString()
    properties[ProducerConfig.BATCH_SIZE_CONFIG] = (32*1024).toString()

    return KafkaProducer(properties)
}
