package bankaccount

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.utils.Bytes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.kstream.Materialized
import org.apache.kafka.streams.kstream.Printed
import org.apache.kafka.streams.state.KeyValueStore
import java.util.*


private const val GROUP_ID = "bank-account-balance-stream"
private const val BANK_ACCOUNT_BALANCE_TOPIC = "bank-account-balance"

fun main() {
    val properties = Properties()
    properties[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:9092"
    properties[StreamsConfig.APPLICATION_ID_CONFIG] = GROUP_ID
    properties[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[StreamsConfig.PROCESSING_GUARANTEE_CONFIG] = StreamsConfig.EXACTLY_ONCE
    properties[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "earliest"

    val bankAccountBalanceSerde = jsonPojoSerde<BankAccountBalance>()

    val builder = StreamsBuilder()
    val bankAccountBalanceMaterializer = Materialized
            .`as`<String, BankAccountBalance, KeyValueStore<Bytes, ByteArray>>(BANK_ACCOUNT_BALANCE_TOPIC)
            .withKeySerde(Serdes.String())
            .withValueSerde(bankAccountBalanceSerde)

    val balanceTable = builder.stream<String, String>(BANK_TOPIC)
            .mapValues { v -> OBJECT_MAPPER.readValue(v, BankAccountTransaction::class.java)  }
            .groupByKey()
            .aggregate({ BankAccountBalance() }, { _, value, aggregate ->
                aggregate.add(value.amount, value.date)
                aggregate
            }, bankAccountBalanceMaterializer)

    val balanceStream = balanceTable.toStream()
    balanceStream.print(Printed.toSysOut())
    balanceStream.to(BANK_ACCOUNT_BALANCE_TOPIC)

    val topology = builder.build()
    println(topology.describe())

    val kafkaStreams = KafkaStreams(topology, properties)
    kafkaStreams.start()
    Runtime.getRuntime().addShutdownHook(Thread(kafkaStreams::close))
}