package bankaccount

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.serialization.Serializer

class JsonPOJODeserializer<T>(private val mapper: ObjectMapper,
                              private var clazz: Class<T>): Deserializer<T> {
    override fun configure(props: Map<String?, *>, isKey: Boolean) {}

    override fun deserialize(topic: String, bytes: ByteArray?): T? {
        if (bytes == null) return null
        return try {
            mapper.readValue(bytes, clazz)
        } catch (e: Exception) {
            throw SerializationException(e)
        }
    }

    override fun close() {}
}

class JsonPOJOSerializer<T>(private val mapper: ObjectMapper): Serializer<T> {
    override fun configure(props: Map<String?, *>?, isKey: Boolean) {}
    override fun serialize(topic: String, data: T?): ByteArray? {
        return if (data == null) null else try {
            mapper.writeValueAsBytes(data)
        } catch (e: java.lang.Exception) {
            throw SerializationException("Error serializing JSON message", e)
        }
    }

    override fun close() {}
}

inline fun <reified T> jsonPojoSerde(): Serde<T> {
    val serializer = JsonPOJOSerializer<T>(OBJECT_MAPPER)
    val deserializer = JsonPOJODeserializer(OBJECT_MAPPER, T::class.java)
    return  Serdes.serdeFrom(serializer, deserializer)
}