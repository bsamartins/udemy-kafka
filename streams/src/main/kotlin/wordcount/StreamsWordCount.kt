import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Materialized
import org.apache.kafka.streams.kstream.Printed
import org.apache.kafka.streams.kstream.Produced
import java.util.*
import org.apache.kafka.streams.kstream.Named.`as` as named

fun main() {
    // properties
    val properties = Properties()
    properties[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:9092"
    properties[StreamsConfig.APPLICATION_ID_CONFIG] = "streams-start-app"
    properties[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "earliest"

    // create topology
    val builder = StreamsBuilder()
    val inputTopic = builder.stream<String, String>("word-count-input")
    val table = inputTopic.mapValues { value -> value.toLowerCase() }
            .flatMapValues { value -> value.split(" ") }
            .selectKey { _, value -> value }
            .groupByKey()
            .count(named("counts"), Materialized.with(Serdes.String(), Serdes.Long()))

    val stream = table.toStream()
    stream.print(Printed.toSysOut())
    stream.to("word-count-output", Produced.with(Serdes.StringSerde(), Serdes.LongSerde()))

    val topology: Topology = builder.build()
    println(topology.describe())

    // build topology
    val kafkaStreams = KafkaStreams(topology, properties)

    // start application
    kafkaStreams.start()
    Runtime.getRuntime().addShutdownHook(Thread(kafkaStreams::close))
}