package io.bsamartins.udemy.kafka.starter

import com.fasterxml.jackson.databind.ObjectMapper

val OBJECT_MAPPER = ObjectMapper()