package io.bsamartins.udemy.kafka.starter.es

import io.bsamartins.udemy.kafka.starter.OBJECT_MAPPER
import org.apache.http.HttpHost
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer
import org.elasticsearch.ElasticsearchException
import org.elasticsearch.action.bulk.BulkRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.CreateIndexRequest
import org.elasticsearch.client.indices.GetIndexRequest
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.rest.RestStatus
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*

object ElasticSearchConsumer {

    private val logger = LoggerFactory.getLogger(ElasticSearchConsumer::class.java)
    private const val hostname = "localhost"
    private const val indexName = "twitter"

    fun run() {
        val client = createClient()
        ensureIndex(client)

        val consumer = createConsumer()

        while (true) {
            val records = consumer.poll(Duration.ofMillis(1000))
            logger.info("Got {} records", records.count())
            val requests = records.map { record ->
                val tweet = record.value()
                val id = getId(tweet)
                logger.info(id)
                return@map IndexRequest(indexName)
                        .source(tweet, XContentType.JSON)
                        .id(id)
            }

            if (requests.isNotEmpty()) {
                val bulkRequest = BulkRequest()
                bulkRequest.add(requests)
                client.bulk(bulkRequest, RequestOptions.DEFAULT)
            }

            logger.info("Committing offsets...")
            consumer.commitSync()
            logger.info("Offsets have been committed")
        }

        client.close()
    }

    private fun createClient(): RestHighLevelClient {
        val builder = RestClient.builder(HttpHost(hostname, 9200))
        return RestHighLevelClient(builder)
    }

    private fun createConsumer(): KafkaConsumer<String, String> {
        val bootstrapServers = "localhost:9092"
        val groupId = "kafka-demo-elasticsearch"
        val topic = "twitter_tweets"

        // consumer configs
        val properties = Properties()
        properties[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapServers
        properties[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.qualifiedName
        properties[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.qualifiedName
        properties[ConsumerConfig.GROUP_ID_CONFIG] = groupId
        properties[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "earliest"
        properties[ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG] = false.toString()
        properties[ConsumerConfig.MAX_POLL_RECORDS_CONFIG] = 100.toString()

        val consumer = KafkaConsumer<String, String>(properties)
        consumer.subscribe(listOf(topic))
        return consumer
    }

    private fun getId(tweet: String): String {
        return OBJECT_MAPPER.readTree(tweet)["id_str"].asText()
    }

    private fun ensureIndex(client: RestHighLevelClient) {
        val indicesClient = client.indices()

        kotlin.runCatching {
            val getIndexRequest = GetIndexRequest(indexName)
            indicesClient.get(getIndexRequest, RequestOptions.DEFAULT)
        }.onFailure { ex ->
            if (ex is ElasticsearchException && ex.status() == RestStatus.NOT_FOUND) {
                val createIndexRequest = CreateIndexRequest(indexName)
                indicesClient.create(createIndexRequest, RequestOptions.DEFAULT)
            } else {
                throw ex
            }
        }

    }

}

fun main() {
    ElasticSearchConsumer.run()
}