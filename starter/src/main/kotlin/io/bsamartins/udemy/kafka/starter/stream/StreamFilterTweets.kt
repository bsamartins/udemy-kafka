
import io.bsamartins.udemy.kafka.starter.OBJECT_MAPPER
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import java.util.*

fun main() {
    // properties
    val properties = Properties()
    properties[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:9092"
    properties[StreamsConfig.APPLICATION_ID_CONFIG] = "demo-kafka-streams"
    properties[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name
    properties[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = Serdes.StringSerde::class.java.name

    // create topology
    val builder = StreamsBuilder()
    val inputTopic = builder.stream<String, String>("twitter_tweets")
    val filteredStream = inputTopic.filter { _, tweet ->
        val followersCount = OBJECT_MAPPER.readTree(tweet)["user"]["followers_count"].asInt()
        return@filter followersCount > 10000
    }
    filteredStream.to("twitter_important")
    // build topology
    val kafkaStreams = KafkaStreams(builder.build(), properties)
    // start application
    kafkaStreams.start()
}