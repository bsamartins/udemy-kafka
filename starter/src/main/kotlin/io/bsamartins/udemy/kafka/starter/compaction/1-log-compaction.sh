#!/bin/bash

# Create topic
kafka-topics --zookeper 127.0.0.1 --create --topic employee-salary --partitions 1 --replication-factor 1 \
  --config cleanup.policy=compact --config min.cleaneable.dirty.ratio=0.001 --config segment.ms=5000

# Describe topic configs
kafka-topics --zookeper 127.0.0.1:2181 --describe --topic employee-salary

# Start consumer in separate terminal
kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic employee-salary --from-beginning \
  --property print.key=true --property key.separator=,

# Producer
kafka-console-producer --broker-list 127.0.0.1:9092 --topic employee-salary --property parse.key=true \
  --property key.separator=,