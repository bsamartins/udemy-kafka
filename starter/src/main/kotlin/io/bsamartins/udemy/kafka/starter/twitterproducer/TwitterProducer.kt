package io.bsamartins.udemy.kafka.starter.twitter

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.LoggerFactory
import twitter4j.TwitterObjectFactory
import twitter4j.TwitterStream
import twitter4j.TwitterStreamFactory
import twitter4j.auth.AccessToken
import twitter4j.conf.ConfigurationBuilder
import java.util.*


object TwitterProducer {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val consumerKey = System.getenv("twitter_consumer-key")!!
    private val consumerSecret = System.getenv("twitter_consumer-secret")!!
    private val token = System.getenv("twitter_token")!!
    private val secret = System.getenv("twitter_token-secret")!!

    fun run() {
        val stream = createTwitterClient()
        val producer = createKafkaProducer()
        Runtime.getRuntime().addShutdownHook(Thread {
            logger.info("Stopping application...")
            stream.shutdown()
            producer.close()
        })
        stream.onStatus { msg ->
            val rawTweet = TwitterObjectFactory.getRawJSON(msg)
            logger.debug("Tweet: {}", rawTweet)
            producer.send(ProducerRecord("twitter_tweets", msg.id.toString(), rawTweet)) { _, err ->
                err?.printStackTrace()
            }
        }
        stream.sample()
    }

    private fun createTwitterClient(): TwitterStream {
        val config = ConfigurationBuilder()
                .setJSONStoreEnabled(true)
                .build()

        val twitterStream = TwitterStreamFactory(config).instance
        twitterStream.setOAuthConsumer(consumerKey, consumerSecret)
        twitterStream.oAuthAccessToken = AccessToken(this.token, this.secret)
        return twitterStream
    }

    private fun createKafkaProducer(): KafkaProducer<String, String> {
        val properties = Properties()
        properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "127.0.0.1:9092"
        properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.qualifiedName
        properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.qualifiedName

        // Safe producer
        properties[ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG] = true.toString()
        properties[ProducerConfig.ACKS_CONFIG] = "all"
        properties[ProducerConfig.RETRIES_CONFIG] = Integer.MAX_VALUE.toString()
        properties[ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION] = 5.toString()

        // compression
        properties[ProducerConfig.COMPRESSION_TYPE_CONFIG] = "snappy"
        properties[ProducerConfig.LINGER_MS_CONFIG] = 20.toString()
        properties[ProducerConfig.BATCH_SIZE_CONFIG] = (32*1024).toString()

        return KafkaProducer(properties)
    }
}

fun main() {
    TwitterProducer.run()
}