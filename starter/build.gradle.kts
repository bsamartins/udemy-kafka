dependencies {
    implementation("org.apache.kafka:kafka-streams")

    implementation("org.twitter4j:twitter4j-core:4.0.7")
    implementation("org.twitter4j:twitter4j-stream:4.0.7")

    implementation("org.elasticsearch.client:elasticsearch-rest-high-level-client:7.6.2")
}